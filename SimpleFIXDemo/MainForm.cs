﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using ExanteFIXWrapper;
using System.Collections.Concurrent;

namespace SimpleFIXDemo
{
    public partial class MainForm : Form
    {
        private static QuoteFIXReceiver feedFIX = null;
        private static OrderFIXExecutor executionFIX = null;
        private string FIXIniVendorPath = @"FIX\fix_vendor.ini";
        private string FIXIniBrokerPath = @"FIX\fix_broker.ini";
        private Thread updateConnectionStatus;

        public MainForm()
        {
            InitializeComponent();
            InitMarketDepthControl();
            InitAccountSummaryControl();
            InitOrdersControl();
        }

        private void InitFIXConnection()
        {
            feedFIX = new QuoteFIXReceiver(FIXIniVendorPath);
            executionFIX = new OrderFIXExecutor(FIXIniBrokerPath, OrderStatusUpdate);
            updateConnectionStatus = new Thread(UpdateConnectionStatus);
            updateConnectionStatus.Start();
        }


        private void UpdateConnectionStatus()
        {
            while (true)
            {
                Thread.Sleep(1000);
                if (this.IsHandleCreated)
                {

                    labelConnectionFeedStatusValue.Invoke(new Action(() =>
                    {
                        if (feedFIX != null && feedFIX.IsConnected())
                        {
                            labelConnectionFeedStatusValue.Text = "OK";
                            labelConnectionFeedStatusValue.ForeColor = Color.DarkGreen;
                        }
                        else
                        {
                            labelConnectionFeedStatusValue.Text = "Fail";
                            labelConnectionFeedStatusValue.ForeColor = Color.DarkRed;
                        }
                    }
                    ));
                    labelConnectionBrokerStatus.Invoke(new Action(() =>
                    {
                        if (executionFIX != null && executionFIX.IsConnected())
                        {
                            labelConnectionBrokerStatusValue.Text = "OK";
                            labelConnectionBrokerStatusValue.ForeColor = Color.DarkGreen;
                        }
                        else
                        {
                            labelConnectionBrokerStatusValue.Text = "Fail";
                            labelConnectionBrokerStatusValue.ForeColor = Color.DarkRed;
                        }
                    }
                    ));
                }
            }
        }

        private void KillFIXConnection()
        {
            if (feedFIX != null)
            {
                feedFIX.Dispose();
                feedFIX = null;
                GC.Collect();
            }
            if (executionFIX != null)
            {
                executionFIX.Dispose();
                executionFIX = null;
                GC.Collect();
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            KillFIXConnection();
            InitFIXConnection();
            // Demo - can be removed
            Thread.Sleep(1000);
            SetActiveSymbol(new SymbolBase(@"TEST-RANDOM.TEST"));
        }

        private void buttonDisconnect_Click(object sender, EventArgs e)
        {
            KillFIXConnection();
        }

        private const int WM_SETREDRAW = 0x000B;
        public static void SuspendConrolRedraw(Control control)
        {
            Message msgSuspendUpdate = Message.Create(control.Handle, WM_SETREDRAW, IntPtr.Zero,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgSuspendUpdate);
        }

        public static void ResumeControlRedraw(Control control)
        {
            // Create a C "true" boolean as an IntPtr
            IntPtr wparam = new IntPtr(1);
            Message msgResumeUpdate = Message.Create(control.Handle, WM_SETREDRAW, wparam,
                IntPtr.Zero);

            NativeWindow window = NativeWindow.FromHandle(control.Handle);
            window.DefWndProc(ref msgResumeUpdate);

            control.Invalidate();
        }

        public static void SetDoubleBuffered(System.Windows.Forms.Control c)
        {
            if (System.Windows.Forms.SystemInformation.TerminalServerSession)
                return;

            System.Reflection.PropertyInfo aProp =
                  typeof(System.Windows.Forms.Control).GetProperty(
                        "DoubleBuffered",
                        System.Reflection.BindingFlags.NonPublic |
                        System.Reflection.BindingFlags.Instance);

            aProp.SetValue(c, true, null);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            updateASThread.Abort();
            updateConnectionStatus.Abort();
            KillFIXConnection();            
        }
    }
}

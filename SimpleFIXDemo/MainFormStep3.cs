﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Globalization;
using ExanteFIXWrapper;
using System.Collections.Concurrent;
using System.Threading;
using System.Windows.Forms;


namespace SimpleFIXDemo
{
    partial class MainForm
    {
        /// <summary>
        /// FIX part
        /// </summary>
        /// 
        private String requestIdMassOrderRequest;
        public void MassOrderStatusUpdate(String massStatusRequestID, Boolean isLastStatusMessage)
        {
            if (massStatusRequestID != requestIdMassOrderRequest)
                return;
            RedrawOrders();
        }
        
        public void OrderStatusUpdate(string orderId, OrderStatus orderStatus, Double filledQauntity, Double fillPrice)
        {
            RedrawOrders();
        }

        private void MassOrderStatusRequest()
        {
            try
            {
                this.requestIdMassOrderRequest = System.Guid.NewGuid().ToString();
                executionFIX.MassOrderStatusRequest(requestIdMassOrderRequest, MassOrderStatusUpdate);
            }
            catch (Exception)
            {
                // not connected?
            }
        }

        /// <summary>
        /// UI part
        /// </summary>
        /// 
        private void InitOrdersControl()
        {
            listViewOrders.Columns.Add(@"ClientOrderId", 120);
            listViewOrders.Columns.Add(@"ExanteId", 100);
            listViewOrders.Columns.Add("Side");
            listViewOrders.Columns.Add("Type");
            listViewOrders.Columns.Add("Quantity");
            listViewOrders.Columns.Add("Price");
            listViewOrders.Columns.Add("Status", 100);
            listViewOrders.Sorting = SortOrder.Descending;
        }

        public void RedrawOrders()
        {
            labelStatusValue.Invoke(new Action(() => { labelStatusValue.Text = "OK"; labelStatusValue.ForeColor = Color.DarkGreen; }));
            listViewOrders.Invoke(new Action(() =>
            {
                SuspendConrolRedraw(listViewOrders);
                listViewOrders.Items.Clear();
                foreach (OrderBase order in executionFIX.GetActiveOrdersCopy())
                {
                    String orderType = "Market";
                    String price = "";
                    if (order.GetType() == typeof(OrderLimit))
                    {
                        orderType = "Limit";
                        price = ((OrderLimit)order).Price.ToString();
                    }
                    ListViewItem row = new ListViewItem(new string[] { order.ClientOrderId, order.Symbol.EXANTEId, order.Position.ToString(), orderType, order.Quantity.ToString(), price, order.OrderStatus.ToString() });
                    listViewOrders.Items.Add(row);
                }
                listViewOrders.Sort();
                ResumeControlRedraw(listViewOrders);
            }
            ));
        }


        private void buttonMassOrderStatusRequest_Click(object sender, EventArgs e)
        {
            labelStatusValue.Invoke(new Action(() => { labelStatusValue.Text = "Requesting..."; labelStatusValue.ForeColor = Color.Yellow; }));
            MassOrderStatusRequest();
        }


        private void listViewOrders_DoubleClick(object sender, EventArgs e)
        {
            executionFIX.cancelOrder(listViewOrders.SelectedItems[0].SubItems[0].Text);
        }
    }
}
